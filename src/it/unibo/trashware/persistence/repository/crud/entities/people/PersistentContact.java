package it.unibo.trashware.persistence.repository.crud.entities.people;

import java.util.Set;

import it.unibo.trashware.persistence.model.people.Contact;
import it.unibo.trashware.persistence.repository.exception.BoundedReferenceException;
import it.unibo.trashware.persistence.repository.exception.DuplicateKeyValueException;
import it.unibo.trashware.persistence.repository.exception.NonExistentReferenceException;
import it.unibo.trashware.persistence.repository.query.criteria.QueryObject;

/**
 * The interface modeling the four CRUD operations for the domain entity of
 * {@link Contact}.
 * 
 * @author Manuel Bonarrigo
 */
public interface PersistentContact {

    /**
     * The CRUD operation of proposing a new {@link Contact} to be created.
     * 
     * @param contact
     *            the Contact to be created
     * @throws DuplicateKeyValueException
     *             if the Contact to be created is already present, and the
     *             persistence storage does not tolerate value repetition.
     * @throws NonExistentReferenceException
     *             if any referenced value is absent or malformed, and the
     *             persistence storage was not able to precisely locate it.
     */
    void createEntry(Contact contact) throws NonExistentReferenceException, DuplicateKeyValueException;

    /**
     * The CRUD operation of requesting a {@link Set} of {@link Contact} filtered by
     * the conditions of the {@link QueryObject}.
     * 
     * @param filter
     *            the filter that will determine which results are going to be
     *            fetched
     * @return A Set containing all the Contact objects matched against the filter
     */
    Set<Contact> readContact(QueryObject filter);

    /**
     * The CRUD operation of proposing a {@link Contact} to be updated with the
     * value of a new one.
     * 
     * @param oldContact
     *            the Contact actually stored.
     * @param newContact
     *            the Contact with the informations to be fetched for update.
     * @throws DuplicateKeyValueException
     *             if the Contact update transforms the entity in an already
     *             present one, and the persistence storage does not tolerate value
     *             repetition.
     * @throws NonExistentReferenceException
     *             if any referenced value is absent or malformed, and the
     *             persistence storage was not able to precisely locate it.
     */
    void updateEntry(Contact oldContact, Contact newContact) throws NonExistentReferenceException, 
            DuplicateKeyValueException;

    /**
     * The CRUD operation of proposing a {@link Contact} for deletion.
     * 
     * @param contact
     *            the Contact to be deleted from the persistent data.
     * @throws NonExistentReferenceException
     *             if any referenced value is absent or malformed, and the
     *             persistence storage was not able to precisely locate it.
     * @throws BoundedReferenceException
     *             if exists any entity actively referencing this one, making thus
     *             this one impossible to delete.
     */
    void deleteEntry(Contact contact) throws NonExistentReferenceException, BoundedReferenceException;
}
