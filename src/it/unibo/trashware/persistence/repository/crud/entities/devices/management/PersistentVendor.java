package it.unibo.trashware.persistence.repository.crud.entities.devices.management;

import java.util.Set;

import it.unibo.trashware.persistence.model.devices.Vendor;
import it.unibo.trashware.persistence.repository.exception.BoundedReferenceException;
import it.unibo.trashware.persistence.repository.exception.DuplicateKeyValueException;
import it.unibo.trashware.persistence.repository.exception.NonExistentReferenceException;
import it.unibo.trashware.persistence.repository.query.criteria.QueryObject;

/**
 * The interface modeling the four CRUD operations for the domain entity of
 * {@link Vendor}.
 * 
 * @author Manuel Bonarrigo
 */
public interface PersistentVendor {
    /**
     * The CRUD operation of proposing a new {@link Vendor} to be created.
     * 
     * @param vendor
     *            the Vendor to be created.
     * @throws DuplicateKeyValueException
     *             if the Vendor to be created is already present, and the
     *             persistence storage does not tolerate value repetition.
     * @throws NonExistentReferenceException
     *             if any referenced value is absent or malformed, and the
     *             persistence storage was not able to precisely locate it.
     * 
     */
    void createEntry(Vendor vendor) throws NonExistentReferenceException, DuplicateKeyValueException;

    /**
     * The CRUD operation of requesting a {@link Set} of {@link Vendor}
     * filtered by the conditions of the {@link QueryObject}.
     * 
     * @param filter
     *            the filter that will determine which results are going to be
     *            fetched
     * @return A Set containing all the Vendor objects matched against the
     *         filter
     */
    Set<Vendor> readVendor(QueryObject filter);

    /**
     * The CRUD operation of proposing a {@link Vendor} to be updated with
     * the value of a new one.
     * 
     * @param oldVendor
     *            the Vendor actually stored.
     * @param newVendor
     *            the Vendor with the informations to be fetched for
     *            update.
     * @throws DuplicateKeyValueException
     *             if the Vendor update transforms the entity in an already
     *             present one, and the persistence storage does not tolerate value
     *             repetition.
     * @throws NonExistentReferenceException
     *             if any referenced value is absent or malformed, and the
     *             persistence storage was not able to precisely locate it.
     * 
     */
    void updateEntry(Vendor oldVendor, Vendor newVendor) throws NonExistentReferenceException, 
            DuplicateKeyValueException;

    /**
     * The CRUD operation of proposing a {@link Vendor} for deletion.
     * 
     * @param vendor
     *            the Vendor to be deleted from the persistent data.
     * @throws NonExistentReferenceException
     *             if any referenced value is absent or malformed, and the
     *             persistence storage was not able to precisely locate it.
     * @throws BoundedReferenceException
     *             if exists any entity actively referencing this one, making thus
     *             this one impossible to delete.
     */
    void deleteEntry(Vendor vendor) throws NonExistentReferenceException, BoundedReferenceException;

}
