/*
 * This file is generated by jOOQ.
*/
package it.unibo.trashware.persistence.repository.mapper.jooq.trustalodb.trustalo.tables;


import java.util.Arrays;
import java.util.List;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.Identity;
import org.jooq.Index;
import org.jooq.Name;
import org.jooq.Schema;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.UniqueKey;
import org.jooq.impl.DSL;
import org.jooq.impl.TableImpl;
import org.jooq.types.UByte;

import it.unibo.trashware.persistence.repository.mapper.jooq.trustalodb.trustalo.Indexes;
import it.unibo.trashware.persistence.repository.mapper.jooq.trustalodb.trustalo.Keys;
import it.unibo.trashware.persistence.repository.mapper.jooq.trustalodb.trustalo.Trustalo;
import it.unibo.trashware.persistence.repository.mapper.jooq.trustalodb.trustalo.tables.records.TrashwaredevicecategoriesRecord;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.10.6"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Trashwaredevicecategories extends TableImpl<TrashwaredevicecategoriesRecord> {

    private static final long serialVersionUID = 757648275;

    /**
     * The reference instance of <code>trustalo.trashwaredevicecategories</code>
     */
    public static final Trashwaredevicecategories TRASHWAREDEVICECATEGORIES = new Trashwaredevicecategories();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<TrashwaredevicecategoriesRecord> getRecordType() {
        return TrashwaredevicecategoriesRecord.class;
    }

    /**
     * The column <code>trustalo.trashwaredevicecategories.ID</code>.
     */
    public final TableField<TrashwaredevicecategoriesRecord, UByte> ID = createField("ID", org.jooq.impl.SQLDataType.TINYINTUNSIGNED.nullable(false).identity(true), this, "");

    /**
     * The column <code>trustalo.trashwaredevicecategories.Acronym</code>.
     */
    public final TableField<TrashwaredevicecategoriesRecord, String> ACRONYM = createField("Acronym", org.jooq.impl.SQLDataType.VARCHAR(10).nullable(false), this, "");

    /**
     * The column <code>trustalo.trashwaredevicecategories.Name</code>.
     */
    public final TableField<TrashwaredevicecategoriesRecord, String> NAME = createField("Name", org.jooq.impl.SQLDataType.VARCHAR(50).nullable(false), this, "");

    /**
     * The column <code>trustalo.trashwaredevicecategories.AllowsMultipleComponund</code>.
     */
    public final TableField<TrashwaredevicecategoriesRecord, Byte> ALLOWSMULTIPLECOMPONUND = createField("AllowsMultipleComponund", org.jooq.impl.SQLDataType.TINYINT.nullable(false), this, "");

    /**
     * Create a <code>trustalo.trashwaredevicecategories</code> table reference
     */
    public Trashwaredevicecategories() {
        this(DSL.name("trashwaredevicecategories"), null);
    }

    /**
     * Create an aliased <code>trustalo.trashwaredevicecategories</code> table reference
     */
    public Trashwaredevicecategories(String alias) {
        this(DSL.name(alias), TRASHWAREDEVICECATEGORIES);
    }

    /**
     * Create an aliased <code>trustalo.trashwaredevicecategories</code> table reference
     */
    public Trashwaredevicecategories(Name alias) {
        this(alias, TRASHWAREDEVICECATEGORIES);
    }

    private Trashwaredevicecategories(Name alias, Table<TrashwaredevicecategoriesRecord> aliased) {
        this(alias, aliased, null);
    }

    private Trashwaredevicecategories(Name alias, Table<TrashwaredevicecategoriesRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, "");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Schema getSchema() {
        return Trustalo.TRUSTALO;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Index> getIndexes() {
        return Arrays.<Index>asList(Indexes.TRASHWAREDEVICECATEGORIES_PRIMARY, Indexes.TRASHWAREDEVICECATEGORIES_SIGLA);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Identity<TrashwaredevicecategoriesRecord, UByte> getIdentity() {
        return Keys.IDENTITY_TRASHWAREDEVICECATEGORIES;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UniqueKey<TrashwaredevicecategoriesRecord> getPrimaryKey() {
        return Keys.KEY_TRASHWAREDEVICECATEGORIES_PRIMARY;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<UniqueKey<TrashwaredevicecategoriesRecord>> getKeys() {
        return Arrays.<UniqueKey<TrashwaredevicecategoriesRecord>>asList(Keys.KEY_TRASHWAREDEVICECATEGORIES_PRIMARY, Keys.KEY_TRASHWAREDEVICECATEGORIES_SIGLA);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Trashwaredevicecategories as(String alias) {
        return new Trashwaredevicecategories(DSL.name(alias), this);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Trashwaredevicecategories as(Name alias) {
        return new Trashwaredevicecategories(alias, this);
    }

    /**
     * Rename this table
     */
    @Override
    public Trashwaredevicecategories rename(String name) {
        return new Trashwaredevicecategories(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    public Trashwaredevicecategories rename(Name name) {
        return new Trashwaredevicecategories(name, null);
    }
}
