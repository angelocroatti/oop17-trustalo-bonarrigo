package it.unibo.trashware.persistence.model.devices;

import java.util.Optional;

import it.unibo.trashware.persistence.repository.metamapping.annotations.EntityInterface;
import it.unibo.trashware.persistence.repository.metamapping.annotations.InterfaceMethodToSchemaField;
import it.unibo.trashware.persistence.repository.metamapping.annotations.InterfaceToSchemaEntity;

/**
 * Express the refinement progress of a
 * {@link it.unibo.trashware.persistence.model.devices.RefinedDevice
 * RefinedDevice}.
 * 
 * @author Manuel Bonarrigo
 *
 */
@EntityInterface
@InterfaceToSchemaEntity(schemaEntity = "DeviceStates")
public interface DeviceWorkProgress {

    /**
     * Retrieve the name of the state of the progress.
     * 
     * @return a {@link String} containing the progress name.
     */
    @InterfaceMethodToSchemaField(returnType = String.class, schemaField = "Name")
    String getName();

    /**
     * Retrieve the description of the state of the progress.
     * 
     * @return an {@link Optional} containing the informations, or Optional.empty if
     *         they are unknown.
     */
    @InterfaceMethodToSchemaField(returnType = String.class, schemaField = "Description")
    Optional<String> getDescription();

}
