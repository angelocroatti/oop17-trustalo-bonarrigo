package it.unibo.trashware.persistence.model.requests;

import it.unibo.trashware.persistence.model.devices.GenericDevice;
import it.unibo.trashware.persistence.repository.metamapping.annotations.Carrier;
import it.unibo.trashware.persistence.repository.metamapping.annotations.EntityInterface;
import it.unibo.trashware.persistence.repository.metamapping.annotations.InterfaceMethodToSchemaField;
import it.unibo.trashware.persistence.repository.metamapping.annotations.InterfaceToSchemaEntity;

/**
 * Represents a request about a {@link GenericDevice}.
 * 
 * @author Manuel Bonarrigo
 *
 */
@EntityInterface
@Carrier
@InterfaceToSchemaEntity(schemaEntity = "DeviceRequestsDeviceModels")
public interface GenericDeviceRequest {

    /**
     * Retrieve the GenericDevice bound to a Request.
     * 
     * @return a {@link GenericDevice} associated to the {@link RequestDetail} obtainable
     *         by the method {@code getRequestDetail()}
     */
    @InterfaceMethodToSchemaField(returnType = GenericDevice.class, schemaField = "DeviceModel")
    GenericDevice getDeviceRequested();

    /**
     * Retrieve the Request this class is all about.
     * 
     * @return a {@link RequestDetail}
     */
    @InterfaceMethodToSchemaField(returnType = RequestDetail.class, schemaField = "RequestDevice")
    RequestDetail getRequestDetail();

    /**
     * Retrieve the quantity of {@link GenericDevice} requested by this request.
     * 
     * @return an Integer representing the quantity of GenericDevice requested.
     */
    @InterfaceMethodToSchemaField(returnType = Integer.class, schemaField = "Quantity")
    Integer getQuantityRequested();

}
