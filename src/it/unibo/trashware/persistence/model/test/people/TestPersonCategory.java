package it.unibo.trashware.persistence.model.test.people;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import it.unibo.trashware.persistence.model.people.PersonCategory;
import it.unibo.trashware.persistence.model.people.concreteness.PersonCategoryImpl;
import it.unibo.trashware.persistence.model.test.TestConstants;

/**
 * A test over the construction and equality behaviours of the
 * {@link PersonCategoryImpl} implementation.
 * 
 * @author Manuel Bonarrigo
 *
 */
@RunWith(JUnitPlatform.class)
@SuppressFBWarnings("NP_NULL_PARAM_DEREF_NONVIRTUAL")
public class TestPersonCategory {

    private final PersonCategory personCategory;
    private final PersonCategory differentPersonCategory;
    private final PersonCategory samePersonCategory;

    private final Executable nullParameter = () -> {
        new PersonCategoryImpl(null);
    };
    private final Executable emptyParameter = () -> {
        new PersonCategoryImpl(TestConstants.EMPTY_STRING);
    };
    private final Executable singleEmptyParameter = () -> {
        new PersonCategoryImpl(TestConstants.SINGLE_SPACE_STRING);
    };
    private final Executable multiEmptyParameter = () -> {
        new PersonCategoryImpl(TestConstants.MULTI_SPACE_STRING);
    };

    /**
     * Set up all the objects needed by the test.
     */
    public TestPersonCategory() {
        this.personCategory = new PersonCategoryImpl(TestConstants.A_STRING);
        this.differentPersonCategory = new PersonCategoryImpl(TestConstants.A_DIFFERENT_STRING);
        this.samePersonCategory = new PersonCategoryImpl(TestConstants.THE_SAME_STRING);
    }

    /**
     * Tests over the constructor(s) ability to pursue only the legal initialization.
     */
    @Test
    public void constructionTest() {
        assertTrue(personCategory.getName().equals(TestConstants.A_STRING));

        assertThrows(NullPointerException.class, nullParameter);
        assertThrows(IllegalArgumentException.class, emptyParameter);
        assertThrows(IllegalArgumentException.class, singleEmptyParameter);
        assertThrows(IllegalArgumentException.class, multiEmptyParameter);
    }

    /**
     * Testing of the equals() interactions.
     */
    @Test
    public void equalityTest() {
        assertTrue(personCategory.equals(samePersonCategory));
        assertTrue(samePersonCategory.equals(personCategory));
        assertFalse(personCategory.equals(differentPersonCategory));
        assertFalse(samePersonCategory.equals(differentPersonCategory));
    }
}
