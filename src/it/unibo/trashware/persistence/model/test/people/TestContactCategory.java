package it.unibo.trashware.persistence.model.test.people;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import it.unibo.trashware.persistence.model.people.ContactCategory;
import it.unibo.trashware.persistence.model.people.concreteness.ContactCategoryImpl;
import it.unibo.trashware.persistence.model.test.TestConstants;

/**
 * A test over the construction and equality behaviours of the
 * {@link ContactCategoryImpl} implementation.
 * 
 * @author Manuel Bonarrigo
 *
 */
@RunWith(JUnitPlatform.class)
@SuppressFBWarnings("NP_NULL_PARAM_DEREF_NONVIRTUAL")
public class TestContactCategory {

    private final ContactCategory contactCategory;
    private final ContactCategory differentContactCategory;
    private final ContactCategory sameContactCategory;

    private final Executable nullParameter = () -> {
        new ContactCategoryImpl(null);
    };
    private final Executable emptyParameter = () -> {
        new ContactCategoryImpl(TestConstants.EMPTY_STRING);
    };
    private final Executable singleEmptyParameter = () -> {
        new ContactCategoryImpl(TestConstants.SINGLE_SPACE_STRING);
    };
    private final Executable multiEmptyParameter = () -> {
        new ContactCategoryImpl(TestConstants.MULTI_SPACE_STRING);
    };

    /**
     * Set up all the objects needed by the test.
     */
    public TestContactCategory() {
        this.contactCategory = new ContactCategoryImpl(TestConstants.A_STRING);
        this.differentContactCategory = new ContactCategoryImpl(TestConstants.A_DIFFERENT_STRING);
        this.sameContactCategory = new ContactCategoryImpl(TestConstants.THE_SAME_STRING);
    }

    /**
     * Tests over the constructor(s) ability to pursue only the legal initialization.
     */
    @Test
    public void constructionTest() {
        assertTrue(contactCategory.getName().equals(TestConstants.A_STRING));

        assertThrows(NullPointerException.class, nullParameter);
        assertThrows(IllegalArgumentException.class, emptyParameter);
        assertThrows(IllegalArgumentException.class, singleEmptyParameter);
        assertThrows(IllegalArgumentException.class, multiEmptyParameter);
    }

    /**
     * Testing of the equals() interactions.
     */
    @Test
    public void equalityTest() {
        assertTrue(contactCategory.equals(sameContactCategory));
        assertTrue(sameContactCategory.equals(contactCategory));
        assertFalse(contactCategory.equals(differentContactCategory));
        assertFalse(sameContactCategory.equals(differentContactCategory));
    }
}
