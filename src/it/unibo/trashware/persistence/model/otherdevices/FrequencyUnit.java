package it.unibo.trashware.persistence.model.otherdevices;

import it.unibo.trashware.persistence.repository.metamapping.annotations.EntityInterface;
import it.unibo.trashware.persistence.repository.metamapping.annotations.InterfaceMethodToSchemaField;
import it.unibo.trashware.persistence.repository.metamapping.annotations.InterfaceToSchemaEntity;

/**
 * Represents the processor's clock speed unit of measure.
 * 
 * @author Manuel Bonarrigo
 *
 */
@EntityInterface
@InterfaceToSchemaEntity(schemaEntity = "FrequencyUnits", identifierName = "Name")
public interface FrequencyUnit {

    /**
     * Retrieve the name of the processor clock speed unit of measure.
     * 
     * @return a String representing the name of the unit of measure.
     */
    @InterfaceMethodToSchemaField(returnType = String.class, schemaField = "Name")
    String getName();

}
